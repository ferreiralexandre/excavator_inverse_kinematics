from math import radians, pi
from numpy import mat, cos, sin, block, identity, zeros, array
from numpy.linalg import inv
import math

from scipy.optimize import minimize

#function to return to the point 1700 and 650
def return_origin(a,b,alpha,q): 
    print("return_origin")
    nq=next_point(1700,650,a,b,alpha)
    alpha=path_plan(nq,a,b,alpha)
    
    return alpha

#rotation matrix for the inverse kinematics calculation
def rotation_2dmatrix(q):
    
    R = mat([[cos(q), -sin(q)],[sin(q), cos(q)]])
    
    return R

#function to find the angle after the calculation
def find_angle(sin_theta, cos_theta):
    
    angle_rad = math.atan2(sin_theta, cos_theta)
    
    return angle_rad

#calculation in x
def xE_func(q,xE,yE,alpha,a,b):

    
    R0_w=rotation_2dmatrix(alpha[0])
    R1_0=rotation_2dmatrix(alpha[1])*rotation_2dmatrix(q[0])
    R2_1=rotation_2dmatrix(alpha[2])*rotation_2dmatrix(q[1])
    R3_2=rotation_2dmatrix(alpha[3])*rotation_2dmatrix(q[2])
    R4_3=rotation_2dmatrix(alpha[4])

    P0_w=mat([[a[0]],[b[0]]])
    P1_0=mat([[a[1]],[b[1]]])
    P2_1=mat([[a[2]],[b[2]]])
    P3_2=mat([[a[3]],[b[3]]])
    P4_3=mat([[a[4]],[b[4]]])


    T0_w=block([[R0_w,P0_w],[zeros((1,2)),1]])
    T1_0=block([[R1_0,P1_0],[zeros((1,2)),1]])
    T2_1=block([[R2_1,P2_1],[zeros((1,2)),1]])
    T3_2=block([[R3_2,P3_2],[zeros((1,2)),1]])
    T4_3=block([[R4_3,P4_3],[zeros((1,2)),1]])


    T1_w=T0_w*T1_0
    T2_w=T1_w*T2_1
    T3_w=T2_w*T3_2
    T4_w=T3_w*T4_3

 
    func_x = T4_w[0,2] - (xE)

    return func_x

#calculation in Y
def yE_func(q,xE,yE,alpha,a,b):

    R0_w=rotation_2dmatrix(alpha[0])
    R1_0=rotation_2dmatrix(alpha[1])*rotation_2dmatrix(q[0])
    R2_1=rotation_2dmatrix(alpha[2])*rotation_2dmatrix(q[1])
    R3_2=rotation_2dmatrix(alpha[3])*rotation_2dmatrix(q[2])
    R4_3=rotation_2dmatrix(alpha[4])

    P0_w=mat([[a[0]],[b[0]]])
    P1_0=mat([[a[1]],[b[1]]])
    P2_1=mat([[a[2]],[b[2]]])
    P3_2=mat([[a[3]],[b[3]]])
    P4_3=mat([[a[4]],[b[4]]])


    T0_w=block([[R0_w,P0_w],[zeros((1,2)),1]])
    T1_0=block([[R1_0,P1_0],[zeros((1,2)),1]])
    T2_1=block([[R2_1,P2_1],[zeros((1,2)),1]])
    T3_2=block([[R3_2,P3_2],[zeros((1,2)),1]])
    T4_3=block([[R4_3,P4_3],[zeros((1,2)),1]])


    T1_w=T0_w*T1_0
    T2_w=T1_w*T2_1
    T3_w=T2_w*T3_2
    T4_w=T3_w*T4_3

 
    func_y = T4_w[1,2] - (yE)

    return func_y

#function to calculate the next point
def next_point(xE,yE,a,b, alpha):
    
    print("goal:", xE,yE)
    #print(a,b,alpha)

    cons = ({'type': 'eq', 'fun': xE_func,'args': (xE,yE,alpha,a,b)},
            {'type': 'eq', 'fun': yE_func,'args': (xE,yE,alpha,a,b)},
            )

    fun = lambda q:q[0]**2+q[1]**2+q[2]**2

    q0 = array((0.0, 0.0, 0.0))
    
    bnds = ((-pi/2, pi/2), (-pi/2, pi/2), (-pi/2, pi/2))

    res = minimize(fun, q0, bounds=bnds, constraints=cons)

    print(res)
    success=res.success
    q=[ res.x[0] , res.x[1], res.x[2]]
    
    return q,success

#function to calculate what is the next point to reach
def slope_next_point(slope,xE,yE):
    
    slope=math.degrees(slope)
    xE+= 200*math.sin(slope)
    yE+= -200*math.cos(slope)
    
    return xE,yE

#function to find the inside angles for the joints
def path_plan(q,a,b,alpha):

    R0_w=rotation_2dmatrix(alpha[0])
    R1_0=rotation_2dmatrix(alpha[1])*rotation_2dmatrix(q[0])
    R2_1=rotation_2dmatrix(alpha[2])*rotation_2dmatrix(q[1])
    R3_2=rotation_2dmatrix(alpha[3])*rotation_2dmatrix(q[2])
    R4_3=rotation_2dmatrix(alpha[4])
    
    alpha=[0,find_angle(R1_0[1,0],R1_0[1,1]),find_angle(R2_1[1,0],R2_1[1,1]),find_angle(R3_2[1,0],R3_2[1,1]),0]

    return alpha