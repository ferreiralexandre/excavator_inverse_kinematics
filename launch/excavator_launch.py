from ament_index_python.packages import get_package_share_path
from launch_ros.parameter_descriptions import ParameterValue
from launch_ros.actions import Node
from launch.substitutions import Command, LaunchConfiguration
from launch.actions import DeclareLaunchArgument, ExecuteProcess, OpaqueFunction
from launch import LaunchDescription

import os

def generate_launch_description():

    # Defining the object, which must be returned
    ld = LaunchDescription()

    package_path = get_package_share_path('excavator')
    urdf_model_path = os.path.join(package_path, 'urdf/excavator_model.urdf')
    rviz_file_path = os.path.join(package_path, 'config/config.rviz')

    robot_model_arg = DeclareLaunchArgument(name = 'excavator_model',
                                            default_value = str(urdf_model_path),
                                            description = 'This is my URDF model file definition')

    rviz_arg = DeclareLaunchArgument(name = 'rvizconfig',
                                    default_value = str(rviz_file_path),
                                    description = 'This is the file where RVIZ config is saved')

    robot_description = ParameterValue(Command(['xacro ', LaunchConfiguration('excavator_model')]), value_type = str)

    # Here the main node
    main_node = Node(package = "excavator",
                     name = "excavator_node",
                     executable = "excavator",
                     output = "screen",
                     namespace = None)

    # Here the marker node
    marker_node = Node(package = "excavator",
                     name = "line_marker_publisher",
                     executable = "line_marker_publisher",
                     output = "screen",
                     namespace = None)

    # Here the lookup node
    lookup_node = Node(package = "excavator",
                     name = "lookup_transform_node",
                     executable = "lookup_transform_publisher",
                     output = "screen",
                     namespace = None)

    # Starting the robot state publisher
    robot_state_node = Node(package = "robot_state_publisher",
                            executable = "robot_state_publisher",
                            name = "robot_state_publisher",
                            output = "screen",
                            parameters = [{"robot_description": robot_description}])

    # Rviz Node
    rviz_node = Node(package = "rviz2",
                     executable = "rviz2",
                     name = "rviz2",
                     output = "screen",
                     namespace = None,
                     arguments = ["-d", LaunchConfiguration("rvizconfig")])

    

    # Create our description object

    ld.add_action(main_node)
    ld.add_action(marker_node)
    ld.add_action(lookup_node)
    ld.add_action(robot_model_arg)
    ld.add_action(robot_state_node)
    ld.add_action(rviz_arg)
    ld.add_action(rviz_node)

    return ld