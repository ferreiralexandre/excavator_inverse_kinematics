from setuptools import setup

import glob
import os

package_name = 'excavator'

setup(
    name=package_name,
    version='0.0.1',
    packages=[package_name],
    data_files=[
        ('share/ament_index/resource_index/packages', ['resource/' + package_name]),
        ('share/' + package_name,                     ['package.xml']),
        (os.path.join('share', package_name, 'urdf/meshes'), glob.glob('urdf/meshes/*.dae')),
        (os.path.join('share', package_name, 'urdf'), glob.glob('urdf/excavator_model.urdf')),
        (os.path.join('share', package_name, 'launch'), glob.glob('launch/excavator_launch.py')),
        (os.path.join('share', package_name, 'config'), glob.glob('config/config.rviz')),    
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='ubuntu',
    maintainer_email='ubuntu@todo.todo',
    description='First URDF file',
    license='RWTH Aachen University',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'excavator = excavator.robot:main',
            'line_marker_publisher = excavator.line_marker:main',
            'lookup_transform_publisher = excavator.Lookup_transform:main',
        ],
    },
)
