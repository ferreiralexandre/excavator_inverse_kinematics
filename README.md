# Excavator_Inverse_Kinematics

## Overview

The Excavator_Inverse_Kinematics project is a ROS2 package that provides a simulation for a Excavator robot. This Package allows the user to simulate in a RVIZ enviroment an excavator opening a ditche using inverse kinematics to calculate the next point and droping the material 90 degrees counterclock wise.

## Launch File

The excavator_launch.py: This launch file it is used to initiate the simulation. Once the simulation starts it's necessary to input in another terminal the initial parameters so the excavator might start the movement.

## Description

The main goal of the excavator package is to calculate and go to the desire goal using inverse kinematics to calculate the angles on each joint.
The visualization in RVIZ allows the user to follow each step along the way, and by using a strip line compare each sequence to have a clear view of each movement

## Usage

To use the Excavator package, follow the installation:


## Installation

First of all, clone this repository into the src folder of your workspace

```
cd ~/<your_workspace>/src
git clone git@git.rwth-aachen.de:ferreiralexandre/excavator_inverse_kinematics.git
```
Secondly build your ROS2 workspace to compile the package

```
cd ~/<your_workspace>
colcon build
```
To start the simulation, you need to source your workspace and launch the excavator_launch.py file

```
source ~/install/setup.bash
ros2 launch excavator excavator_launch.py
```


## Input data

To input the initial data, it's necessary to open a new terminal, and build your workspace, and follow the next prompt command, where a negative value in Y means that the first point is underground, and a positive value for your slope it means that the next point to reach is deeper than the previous one. 

```
ros2 topic pub --once /entry std_msgs/msg/String "{data: 'xe: <x_value>, ye: <y_value>, slope: <slope_value_in_degrees>'}"

```

## Dependencies

The excavator package depends on the following ROS2 packages:
- robot_state_publisher : Used to publish the robot's state, joints and links
- joint_state_publisher : Used to publish joints states.


## Additional Notes


Ensure that the required hardware and drivers are installed and properly configured for compatibility with your ROS2 environment. To visualize the robot's movement and configuration, use RVIZ2, ensuring it is correctly installed and configured within your ROS2 environment.

Have a great time experimenting with the Excavator robot simulation and let's open some ditches
